package Tet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static Tet.Figure.SHAPES;
import static Tet.SerializableX.DeSerializableRecord;
import static Tet.SerializableX.SerializableRecord;

public class Tetris extends JFrame {
    // Начало блока констант
    // Заголовок окна
    final String TITLE_OF_PROGRAM = "Tetris";
    // Размер блока в пикселях
    static final int BLOCK_SIZE = 25;
    // Ширина шахты в блоках
    static final int FIELD_WIDTH = 10;
    // Высота шахты в блоках
    static final int FIELD_HEIGHT = 18;
    // Начальное местоположение окна
    final int START_LOCATION = 180;
    // Коды клавиш стрелочек на клавиатуре
    static final int LEFT = 37;
    static final int UP = 38;
    static final int RIGHT = 39;
    static final int DOWN = 40;
    static final int ENTER = 13;
    static final int H = 72;
    static final int SPACE = 32;
    static final int ESC = 27;
    // Начисляемые очки за уничтожение линий 1 - 100 , 2 одновременно 300 и тд
    final int[] SCORES = {100, 300, 700, 1500};
    // Конец блока констант

    // Начало блока переменных
    // Задержка движения
    int showDelay = 300;
    // Массив где содержаться рекорды
    static ArrayList<RecordScore> listRecordScore = new ArrayList(5);
    // Пауза
    static boolean isPause = false;
    // Флаг окончания игры
    static boolean isGameOver = false;
    // Текущий счет
    static int gameScore = 0;
    // Новая игра
    static boolean isNewGame = true;
    // Помощь в обработке клавиш
    static boolean isClick = false;
    // Уровень
    static int gameLevel = 1;
    // Шахта
    static int[][] mine = new int[FIELD_HEIGHT + 1][FIELD_WIDTH];
    // Главное окно
    JFrame frame;
    // Отображение
    Canvas canvas = new Canvas();
    // Генерируем случайную фигуру
    Random random = new Random();
    // Работа с фигурами
    static Figure figure = new Figure();
    // Конец блока переменных


    // Точка входа
    public static void main(String[] args)
    {
        new Tetris().Menu();
    }

    Tetris()
    {
        // Установка заголовка приложения
        setTitle(TITLE_OF_PROGRAM);
        // Возможность только сворачивать и закрывать приложение
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // Размер окна
        setBounds(START_LOCATION, START_LOCATION, FIELD_WIDTH * BLOCK_SIZE * 2, FIELD_HEIGHT * BLOCK_SIZE);
        // Неизменяемое окно
        setResizable(false);
        // Цвет фона
        canvas.setBackground(Color.black);

        // Обработчик клавиш
        addKeyListener(new KeyAdapter()
        {
            public void keyPressed(KeyEvent e)
            {
                    switch (e.getKeyCode())
                    {
                        case DOWN : figure.drop();
                                    break;
                        case UP:    figure.rotate();
                                    break;
                        case LEFT:  figure.move(e.getKeyCode());
                                    break;
                        case RIGHT: figure.move(e.getKeyCode());
                                    break;
                        case SPACE: isClick = true;
                                    break;
                        case ESC :  System.exit(0);
                                    break;
                        case H :   { if (isPause == false)
                                    {
                                        isPause = true;
                                    }
                                    else
                                    {
                                        isPause = false;
                                    }
                                    break;}
                    }
                canvas.repaint();
            }
        });
        //
        add(BorderLayout.CENTER, canvas);

        // Видимость окна
        setVisible(true);


        // Инициализация шахты
        Arrays.fill(mine[FIELD_HEIGHT], 1);

        for (int i =0 ; i<FIELD_HEIGHT;i++)
            for(int j = 0; j < FIELD_WIDTH;j++)
                mine[i][j] = 0;

    }

    void Menu()
    {
        for(;;)
        {
            Start();
        }
    }

    void frame_new_game()
    {
        while (isClick == false)
        {
            canvas.repaint();
            try
            {
                Thread.sleep(100);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    void Start()
    {
        int type = 0;

        DeSerializableRecord();

        frame_new_game();
        // Если новая игра создаем первую фигуру и определяем следующую
        if (isNewGame == true )
        {
            isNewGame = false;
            isClick = false;

            type = random.nextInt(7);
            figure = new Figure(type);
            type = random.nextInt(7);
            figure.createNextShape(type);

            canvas.repaint();
        }

        while (!isGameOver)
        {
            // Задержка игры
            Arrays.fill(mine[FIELD_HEIGHT], 1);
            while (isPause) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {}
            }
            try
            {
                Thread.sleep(showDelay);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            canvas.repaint();
            /* Если фигура коснулась земли
             *      Оставляем ее в текущем положении
             *      Проверяем на заполнение линии
             *      Проверяем на поднятие уровня
             *      Создаем новую фигуру
             *      Проверяем коснулась ли фигура верха шахты , если да конец игры
             * Если фигура не коснулась дна
             *      Опускаем фигуру на одну клетку вниз
             */
            if (figure.isTouchGround()) {
                figure.leaveOnTheGround();
                checkFilling();
                levelUp();
                figure = new Figure(type);
                isGameOver = figure.isCrossGround();
                type = random.nextInt(7);
                figure.createNextShape(type);
            } else
                figure.stepDown();
        }
       SerializableRecord();
       resetgame();
    }

    // Новая игра
    void resetgame()
    {
        while (isClick == false)
        {
            // Ждем пока наждум ПРОБЕЛ или ЕСКЕЙТ
            canvas.repaint();
        }
        isClick = false;
        isNewGame = true;
        isGameOver = false;
        gameScore = 0;
        gameLevel = 1;

        for (int i =0 ; i<FIELD_HEIGHT;i++)
            for(int j = 0; j < FIELD_WIDTH;j++)
                mine[i][j] = 0;

    }


    // Поднятие уровня
    void levelUp()
    {
        if (gameScore <= 500)
        {
            gameLevel = 1;
            showDelay = 300;
        }
        if ((gameScore > 500) && (gameScore <= 1000))
        {
            gameLevel = 2;
            showDelay = 280;
        }
        if ((gameScore > 1000) && (gameScore <= 2000))
        {
            gameLevel = 3;
            showDelay = 260;
        }

    }

    // Проверка на заполненость линии , если да удаляем ее и начисляем очки
    void checkFilling() {
        int row = FIELD_HEIGHT - 1;
        int countFillRows = 0;
        while (row > 0) {
            int filled = 1;
            for (int col = 0; col < FIELD_WIDTH; col++)
                filled *= Integer.signum(mine[row][col]);
            if (filled > 0) {
                countFillRows++;
                for (int i = row; i > 0; i--) System.arraycopy(mine[i-1], 0, mine[i], 0, FIELD_WIDTH);
            } else
                row--;
        }
        if (countFillRows > 0) {
            gameScore += SCORES[countFillRows - 1];
        }
    }
}