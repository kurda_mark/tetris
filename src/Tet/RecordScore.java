package Tet;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kurdamark on 25.3.17.
 */
public class RecordScore implements Serializable
{
    public int gameRecordScore ;
    public String userName ;
    public Date dateRecord ;

    RecordScore(int gameRecordScore,String userName,Date dateRecord)
    {
        this.gameRecordScore = gameRecordScore;
        this.userName = userName;
        this.dateRecord = dateRecord;
    }
}
