package Tet;

import java.io.*;
import java.util.*;

import static Tet.Tetris.*;

/**
 * Created by kurdamark on 25.3.17.
 */
public class SerializableX
{
    static String filename = "/home/kurdamark/BSUIR/JAVA/RecordScore.dat";


    static class ScoreComparator implements Comparator<RecordScore> {
        @Override
        public int compare(RecordScore a, RecordScore b) {
            if (a.gameRecordScore < b.gameRecordScore)
                return 1;
            else
                return -1;
        }
    }

    static void file_new()
    {
        File myFile = new File(filename);
        if(myFile.exists())
            System.out.println("Файл существует");
        else {
            File newFile = new File(filename);
            try
            {
                boolean created = newFile.createNewFile();
                if(created)
                    System.out.println("Файл создан");
            }
            catch(IOException ex){

                System.out.println(ex.getMessage());
            }
        }
    }

    static void DeSerializableRecord()
    {
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename)))
        {
            listRecordScore=(ArrayList<RecordScore>)ois.readObject();
            System.out.println("Десереализирован");
        }
        catch(Exception ex){

            System.out.println(ex.getMessage());
        }
    }

    static void SerializableRecord()
    {
        file_new();

        if (listRecordScore.size() < 5)
        {
            Date dateRecord = new Date();
            RecordScore Scorex = new RecordScore(gameScore,"User",dateRecord);
            listRecordScore.add(Scorex);
        }
        else
        {
            if (listRecordScore.get(4).gameRecordScore < gameScore)
            {
                Date dateRecord = new Date();
                RecordScore Scorex = new RecordScore(gameScore,"User",dateRecord);
                listRecordScore.remove(4);
                listRecordScore.add(Scorex);
            }
        }
        // Сортируем коллекцию
        Collections.sort(listRecordScore, new ScoreComparator());
        // Серелизуем
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(listRecordScore);
            System.out.println("Запись произведена");
        } catch (Exception ex) {

            System.out.println(ex.getMessage());
        }
    }
}
