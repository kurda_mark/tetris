package Tet;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Random;

import static Tet.Figure.SHAPES;
import static Tet.Tetris.*;

/**
 * Created by kurdamark on 25.3.17.
 */

// Прорисовка игры
class Canvas extends JPanel
{
    // Начало информациооной панели по оси X
    int startX = FIELD_WIDTH * BLOCK_SIZE + 15;
    Random random = new Random();
    // Сообщение об окончании игры
    final int[][] GAME_OVER_MSG = {
            {0,1,1,0,0,0,1,1,0,0,0,1,0,1,0,0,0,1,1,0},
            {1,0,0,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,1},
            {1,0,1,1,0,1,1,1,1,0,1,0,1,0,1,0,1,1,1,1},
            {1,0,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,0},
            {0,1,1,0,0,1,0,0,1,0,1,0,1,0,1,0,0,1,1,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,1,1,0,0,1,0,0,1,0,0,1,1,0,0,1,1,1,0,0},
            {1,0,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0},
            {1,0,0,1,0,1,0,1,0,0,1,1,1,1,0,1,1,1,0,0},
            {1,0,0,1,0,1,1,0,0,0,1,0,0,0,0,1,0,0,1,0},
            {0,1,1,0,0,1,0,0,0,0,0,1,1,0,0,1,0,0,1,0}};

    final int[][] NEW_GAME_MSG = {
            {0,0,1,0,0,0,1,0,0,1,1,0,0,1,0,0,0,1,0,0},
            {0,0,1,1,0,0,1,0,1,0,0,1,0,1,0,0,0,1,0,0},
            {0,0,1,0,1,0,1,0,1,1,1,1,0,1,0,1,0,1,0,0},
            {0,0,1,0,0,1,1,0,1,0,0,0,0,1,1,0,1,1,0,0},
            {0,0,1,0,0,0,1,0,0,1,1,0,0,1,0,0,0,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,1,1,0,0,0,1,1,0,0,0,1,0,1,0,0,0,1,1,0},
            {1,0,0,0,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,1},
            {1,0,1,1,0,1,1,1,1,0,1,0,1,0,1,0,1,1,1,1},
            {1,0,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,0,0,0},
            {0,1,1,0,0,1,0,0,1,0,1,0,1,0,1,0,0,1,1,0},
    };

    // Переопределение функции paint
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        // Размещение элементов в столбик по Y на одинаковом расстоянии
        int offset = 25;

        // Новая игра
        if (isNewGame == true)
        {
            print_newgame(g);
            print_line(g);
            print_infirmation_newgame(g);
        }
        else
        {
            // Колодец и падающие фигуры
            print_figure(g);
            // Конец игры
            print_gameover(g);
            // Линия разделяющая левую и правую панель
            print_line(g);
            // Отображение следующей фигуры
            print_next_map(g,offset);
            // Информационная часть
            print_information(g,offset);
        }
    }

    // Линия разделяющая левую и правую панель
    void print_line(Graphics g)
    {
        g.setColor(Color.white);
        g.drawLine(startX - 14,0,startX - 14,FIELD_HEIGHT * BLOCK_SIZE);
    }

    //  Колодец и падающие фигуры
    void print_figure(Graphics g)
    {
        for (int x = 0; x < FIELD_WIDTH; x++)
            for (int y = 0; y < FIELD_HEIGHT; y++)
            {
                if (mine[y][x] > 0)
                {
                    g.setColor(new Color(mine[y][x]));
                    g.fill3DRect(x*BLOCK_SIZE+1, y*BLOCK_SIZE+1, BLOCK_SIZE-1, BLOCK_SIZE-1, true);
                }
            }
    }

    // Конец игры
    void print_gameover(Graphics g)
    {
        if (isGameOver) {
            g.setColor(Color.white);
            for (int y = 0; y < GAME_OVER_MSG.length; y++)
                for (int x = 0; x < GAME_OVER_MSG[y].length; x++)
                    if (GAME_OVER_MSG[y][x] == 1) g.fill3DRect(x*11+18, y*11+160, 10, 10, true);
            g.drawString("Press SPACE to start playing", 35, 350);
            g.drawString("OR", 110, 375);
            g.drawString("Press ESC to exit the game", 35, 400);
        } else
            figure.paint(g);

    }


    void print_infirmation_newgame(Graphics g)
    {
        int offset = 25;
        g.setColor(Color.white);
        g.drawString("ESC - Exit ", startX, offset);
        g.drawString("H - Pause ", startX, offset += 25);
        g.drawString("SPACE -  Сonfirmation", startX, offset += 25);
        g.drawString("UP - Rotate", startX, offset += 25);
        g.drawString("DOWN - Drop", startX, offset += 25);
        g.drawString("LEFT - Move left", startX, offset += 25);
        g.drawString("RIGHT - Move Right", startX, offset += 25);
        g.drawLine(startX - 14,offset+=25,FIELD_WIDTH * BLOCK_SIZE * 2,offset);

        SimpleDateFormat ft = new SimpleDateFormat ("dd.MM.yyyy");
        g.drawString("  Name         Score         Date     ", startX, offset += 25);
        for (int i=0; i<listRecordScore.size();i++)
        {
            RecordScore Scorex ;
            Scorex = listRecordScore.get(i);
            g.drawString(Scorex.userName + "           " + Scorex.gameRecordScore + "          " + ft.format(Scorex.dateRecord), startX, offset += 25);
        }
    }

    //
    void print_newgame(Graphics g)
    {
        int color;
        for (int y = 0; y < NEW_GAME_MSG.length; y++)
        {
            for (int x = 0; x < NEW_GAME_MSG[y].length; x++) {
                color = random.nextInt(7);
                g.setColor(new Color(SHAPES[color][4][1]));
                if (NEW_GAME_MSG[y][x] == 1) g.fill3DRect(x * 11 + 18, y * 11 + 160, 10, 10, true);
            }
        }
        g.setColor(Color.white);
        g.drawString("Press SPACE to start playing", 35, 350);
        g.drawString("OR", 110, 375);
        g.drawString("Press ESC to exit the game", 35, 400);
    }

    // Информационное окно
    void print_information(Graphics g,int offset)
    {
        g.setColor(Color.white);
        g.drawString("Score : " + gameScore, startX, offset += 150);
        g.drawString("Level : " + gameLevel, startX, offset += 25);
        g.drawString("Record Score : " + listRecordScore.get(4).gameRecordScore, startX, offset += 25);
    }

    // Отображение следующей фигуры
    void print_next_map(Graphics g,int offset)
    {
        g.drawString("Next Piece : ",startX,offset);
        g.setColor(new Color(Figure.tempColor));
        for (int y = 0; y < Figure.temp.length ; y++)
            for (int x = 0; x < Figure.temp[y].length; x++)
                if (Figure.temp[y][x] == 1) g.fill3DRect(x*BLOCK_SIZE+1+350, y*BLOCK_SIZE+1+offset, BLOCK_SIZE-1, BLOCK_SIZE-1, true);
    }
}
