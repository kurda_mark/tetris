package Tet;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

import static Tet.Tetris.*;

public class Figure
{
    // 7 двумерных фигур тетриса , каждая со своей длинной и стандартным цветом
    static final int[][][] SHAPES = {
            {{0,0,0,0}, {1,1,1,1}, {0,0,0,0}, {0,0,0,0}, {4, 0x00f0f0}}, // I
            {{0,0,0,0}, {0,1,1,0}, {0,1,1,0}, {0,0,0,0}, {4, 0xf0f000}}, // O
            {{1,0,0,0}, {1,1,1,0}, {0,0,0,0}, {0,0,0,0}, {3, 0x0000f0}}, // J
            {{0,0,1,0}, {1,1,1,0}, {0,0,0,0}, {0,0,0,0}, {3, 0xf0a000}}, // L
            {{0,1,1,0}, {1,1,0,0}, {0,0,0,0}, {0,0,0,0}, {3, 0x00f000}}, // S
            {{1,1,1,0}, {0,1,0,0}, {0,0,0,0}, {0,0,0,0}, {3, 0xa000f0}}, // T
            {{1,1,0,0}, {0,1,1,0}, {0,0,0,0}, {0,0,0,0}, {3, 0xf00000}}  // Z
    };
    // Фигура состоит ид блоков
    private ArrayList<Block> figure = new ArrayList<Block>();
    // Временный массив для переворота фигур
    static int[][] shape = new int[4][4];
    //
    static int[][] temp = new int[4][4];
    // Переменные для дипа , длинны и цвета фигуры
    static int type, size, color;
    // Начальные координаты появления фигуры в шахте
    private int x = 3, y = 0;
    //
    static int tempSize,tempColor;


    Figure()
    {

    }

    Figure(int Type)
    {
        type = Type;
        // Получаем ее размер
        size = SHAPES[type][4][0];
        // Получаем ее ецвет
        color = SHAPES[type][4][1];
        if (size == 4) y = -1;

        // Копируем нашу фигуру во временный массив
        for (int i = 0; i < size; i++)
            System.arraycopy(SHAPES[type][i], 0, shape[i], 0, SHAPES[type][i].length);
        createFromShape();
    }


    // Создание следующей фигуры
    void createNextShape(int Type)
    {
        tempSize = SHAPES[Type][4][0];
        tempColor = SHAPES[Type][4][1];
        for (int i = 0; i < tempSize; i++)
            System.arraycopy(SHAPES[Type][i], 0, temp[i], 0, SHAPES[Type][i].length);
    }

    // Создание фигуры
    void createFromShape() {
        for (int x = 0; x < size; x++)
            for (int y = 0; y < size; y++)
                if (shape[y][x] == 1) figure.add(new Block(x + this.x, y + this.y));
    }

    // Проверка на косание с землей
    boolean isTouchGround() {
        for (Block block : figure) if (mine[block.getY() + 1][block.getX()] > 0) return true;
        return false;
    }

    // Проверка на косание в верхом шахты
    boolean isCrossGround() {
        for (Block block : figure) if (mine[block.getY()][block.getX()] > 0) return true;
        return false;
    }

    // Оставляем фигуру в текущем местоположении
    void leaveOnTheGround() {
        for (Block block : figure) mine[block.getY()][block.getX()] = color;
    }

    //
    boolean isTouchWall(int direction) {
        for (Block block : figure) {
            if (direction == LEFT && (block.getX() == 0 || mine[block.getY()][block.getX() - 1] > 0)) return true;
            if (direction == RIGHT && (block.getX() == FIELD_WIDTH - 1 || mine[block.getY()][block.getX() + 1] > 0)) return true;
        }
        return false;
    }

    //
    void move(int direction) {
        if (!isTouchWall(direction)) {
            int dx = direction - 38; // LEFT = -1, RIGHT = 1
            for (Block block : figure) block.setX(block.getX() + dx);
            x += dx;
        }
    }

    // Опускаем фигуру на одну клетку в один цикл
    void stepDown() {
        for (Block block : figure) block.setY(block.getY() + 1);
        y++;
    }

    // Перемещение фигуры вниз до столкновения с землей
    void drop() { while (!isTouchGround()) stepDown(); }

    // Возможность поворота фигуры
    boolean isWrongPosition()
    {
        for (int x = 0; x < size; x++)
            for (int y = 0; y < size; y++)
                if (shape[y][x] == 1) {
                    if (y + this.y < 0) return true;
                    if (x + this.x < 0 || x + this.x > FIELD_WIDTH - 1) return true;
                    if (mine[y + this.y][x + this.x] > 0) return true;
                }
        return false;
    }

    void rotateShape(int direction)
    {
        for (int i = 0; i < size/2; i++)
            for (int j = i; j < size-1-i; j++)
                if (direction == RIGHT)
                {
                    int tmp = shape[size-1-j][i];
                    shape[size-1-j][i] = shape[size-1-i][size-1-j];
                    shape[size-1-i][size-1-j] = shape[j][size-1-i];
                    shape[j][size-1-i] = shape[i][j];
                    shape[i][j] = tmp;
                }
                else
                {
                    int tmp = shape[i][j];
                    shape[i][j] = shape[j][size-1-i];
                    shape[j][size-1-i] = shape[size-1-i][size-1-j];
                    shape[size-1-i][size-1-j] = shape[size-1-j][i];
                    shape[size-1-j][i] = tmp;
                }
    }

    void rotate() {
        rotateShape(RIGHT);
        if (!isWrongPosition()) {
            figure.clear();
            createFromShape();
        } else
            rotateShape(LEFT);
    }

    void paint(Graphics g) {
        for (Block block : figure) block.paint(g, color);
    }
}
